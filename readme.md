# Good Looking (A GNOME Terminal Profile)

- [Fira Code Retina](https://github.com/tonsky/FiraCode/wiki/Linux-instructions) monospaced font with programming ligatures
- Colors taken from Ubuntu TTY (`/etc/console-setup/vtrgb`)

Import profile via `dconf`:
```shell
dconf load /org/gnome/terminal/legacy/profiles:/ < good-looking.dconf
```